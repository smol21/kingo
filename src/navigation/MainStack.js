import { StyleSheet, Text, View } from 'react-native'
import React from 'react';
import InicioKingo from '../views/InicioKingo';
import Premio from '../views/Premio';
import Escaner from '../views/Escaner';
import ReglasBingo from '../views/ReglasBingo';
import Resultados from '../views/Resultados';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

const MainStack = () => {
  return (
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="InicioKingo" component={InicioKingo} />
        <Stack.Screen name="Premio" component={Premio} />
        <Stack.Screen name="Escaner" component={Escaner} />
        <Stack.Screen name="ReglasBingo" component={ReglasBingo} />
        <Stack.Screen name="Resultados" component={Resultados} />
      </Stack.Navigator>
  )
}

export default MainStack
