import { Dimensions, Platform } from 'react-native';
const deviceWidth = Dimensions.get('screen').width;
const deviceHeight = Dimensions.get('screen').height;

const styles = {
// BASE
  scrollViewStyle: {
    flex: 1,
    backgroundColor: '#00000000',
  },
  // Headers
  header: {
    backgroundColor: '#1F16A1',
    alignItems: 'center',
    paddingTop: 20,
    paddingBottom: 20,
  },
  tinyLogo: {
    width: 210,
    height: 90,
  },
  tinyLogoRedes: {
    width: 30,
    height: 30,
  },
  //footer
  footer: {
    backgroundColor: '#1F16A1',
    alignItems: 'center',
    height: 50,
  },
  borderFooter: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
  },
  //BOTONES
  buttonTouchableYellow: {
    fontSize: 25,
    backgroundColor: '#FCED05',
    marginBottom: Platform.OS === 'android' ? 15 : 0,

    width: deviceWidth - 200,
    justifyContent: 'center',
    alignItems: 'center',
    height: 44,
    borderRadius: 50,
  },
  buttonTouchableGrey: {
    fontSize: 25,
    backgroundColor: '#fbf4f4',
    marginBottom: Platform.OS === 'android' ? 15 : 0,

    width: deviceWidth - 200,
    justifyContent: 'center',
    alignItems: 'center',
    height: 44,
    borderRadius: 50,
  },
  buttonTextStyleBlue: {
    color: '#1F16A1',
    fontWeight: 'bold',
    textAlign: 'center',
  },
//textos
textTitle2: {
  fontWeight: 'bold',
  fontSize: 20,
  textAlign: 'center',
  padding: 10,
  color: '#1F16A1',
},
//CUADRICULA
estilosPrincipal: {
  justifyContent: 'center',
  alignItems: 'center',
  marginTop: Platform.OS === 'android' ? 5 : 0,
},
borderCuariculas: {
  flexDirection: 'row',
  marginTop: Platform.OS === 'android' ? 0 : 30,
  // marginBottom: Platform.OS === 'android' ? 30 : 30,
},

estiloNumeros: {
  fontSize: 25,
  fontWeight: 'bold',
  color: '#000',
},
estiloNumeros2: {
  fontSize: 25,
  fontWeight: 'bold',
  color: '#F8CC23',
},
montosPremios: {
  alignSelf: 'center',
  justifyContent: 'flex-start',
  alignItems: 'center',
},

esitloMontos: {
  fontSize: 20,
  fontWeight: '900',
  color: '#1F16A1',
},
esitloMontos2: {
  fontSize: 25,
  fontWeight: '900',
  color: '#1F16A1',
},
esitloMontos3: {
  fontSize: 15,
  fontWeight: '900',
  color: '#1F16A1',
},
esitloMontos4: {
  fontSize: 10,
  fontWeight: '900',
  color: '#1F16A1',
},
esitloMontos5: {
  fontSize: 12,
  fontWeight: '900',
  color: '#1F16A1',
},
tinyLogoMontos: {
  width: 30,
  height: 30,
},


//PRUEBASSSSSS
textTitle:{
  fontSize: 25,
  fontWeight: '900',
  color: '#1F16A1',
},
cardView:{
  padding: 20
},
scanCardView:{
  padding: 20
},
descText:{
  fontSize: 15,
  fontWeight: '900',
  color: '#1F16A1',
},
buttonTouchable:{
  fontSize: 25,
  backgroundColor: '#FCED05',
  marginBottom: Platform.OS === 'android' ? 15 : 0,

  width: deviceWidth - 200,
  justifyContent: 'center',
  alignItems: 'center',
  height: 44,
  borderRadius: 50,
},
buttonTextStyle:{
  fontSize: 15,
  fontWeight: '900',
  color: '#1F16A1',
},
textTitle1:{
  fontSize: 25,
  fontWeight: '900',
  color: '#1F16A1',
},
centerText:{
  fontSize: 15,
  fontWeight: '900',
  color: '#1F16A1',
},
  //DROPDOWN
  dropdown: {
    height: 50,
    borderColor: '#044ebd',
    backgroundColor: '#fff',
    borderWidth: 2,
    borderRadius: 30,
    paddingHorizontal: 8,
    
  },
  placeholderStyle: {
    fontSize: 18,
    color: '#044ebd',
    backgroundColor: 'transparent',
  },
  selectedTextStyle: {
    fontSize: 18,
    color: '#044ebd'
  },
  iconStyle: {
    width: 30,
    height: 30,
  },
  containerStyle:{
    backgroundColor: '#044ebd',
    color: '#fff',
    border: 10
  },
  activeColor:{
    backgroundColor: '#044ebd',
  }
};

export default styles;
