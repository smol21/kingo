import {View, Image, Dimensions, TouchableOpacity, Linking} from 'react-native';
import styles from '../../assets/styles';
import React from 'react';

const SocialNetworks = () => {
    const DeviceWidth = Dimensions.get('window').width;
    return (
      <View style={styles.footer}>
        <View style={styles.borderFooter}>
          <View
            style={{
              flexDirection: 'row',
            }}>
            <View>
              <View
                style={{
                  width: DeviceWidth * 0.19,
                  height: DeviceWidth * 0.15,
                  marginBottom: 1,
                  flex: 1,
                  alignItems: 'center',
                  alignSelf: 'center',
                  justifyContent: 'center',
                }}>
                <TouchableOpacity
                  onPress={() =>
                    Linking.openURL('https://twitter.com/Kingo_oficial')
                  }>
                  <Image
                    style={styles.tinyLogoRedes}
                    source={require('../../assets/twitter.png')}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View>
              <View
                style={{
                  width: DeviceWidth * 0.19,
                  height: DeviceWidth * 0.15,
                  marginBottom: 1,
                  flex: 1,
                  alignItems: 'center',
                  alignSelf: 'center',
                  justifyContent: 'center',
                }}>
                <TouchableOpacity
                  onPress={() =>
                    Linking.openURL('https://www.facebook.com/Kingo_Oficial/')
                  }>
                  <Image
                    style={styles.tinyLogoRedes}
                    source={require('../../assets/facebook.png')}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View>
              <View
                style={{
                  width: DeviceWidth * 0.19,
                  height: DeviceWidth * 0.15,
                  marginBottom: 1,
                  flex: 1,
                  alignItems: 'center',
                  alignSelf: 'center',
                  justifyContent: 'center',
                }}>
                <TouchableOpacity
                  onPress={() =>
                    Linking.openURL(
                      'https://www.instagram.com/kingo_oficialve/',
                    )
                  }>
                  <Image
                    style={styles.tinyLogoRedes}
                    source={require('../../assets/instagram.png')}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View>
              <View
                style={{
                  width: DeviceWidth * 0.19,
                  height: DeviceWidth * 0.15,
                  marginBottom: 1,
                  flex: 1,
                  alignItems: 'center',
                  alignSelf: 'center',
                  justifyContent: 'center',
                }}>
                <TouchableOpacity
                  onPress={() =>
                    Linking.openURL(
                      'https://www.youtube.com/channel/UCahxG23mf9NjeBLMZYXTqow',
                    )
                  }>
                  <Image
                    style={styles.tinyLogoRedes}
                    source={require('../../assets/ICONO-YOUTUBE.png')}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  };

export default SocialNetworks