import React from 'react';
import { View, Text } from 'react-native';
import styles from '../../assets/scanStyle';

const TituloCaduco = (props) => {
    return (
      <View>
        {!props.data && props.valor.length > 0 ? (
          <View style={styles.colorfondoCaduco}>
           <Text style={styles.textTitleCaduco}>
             {props.title}
           </Text>
         </View>
        ) : <View style={{marginTop:10,marginBottom:10}}></View>}
       
      </View>
        
    )
}

export default TituloCaduco;
