import React from 'react';
import {View,Image} from 'react-native';
import styles from '../../assets/styles';

function Header() {
  const image = require('../../assets/header2.jpg');
  return (
    <View style={styles.header}>
      <Image
          source={image}
          resizeMode="cover"
          style={{
            position: 'absolute',
            width: '100%',
            height: 150,
          }}
        />
      <Image style={styles.tinyLogo} source={require('../../assets/logKingo.png')} />
    </View>
  )
}

export default Header