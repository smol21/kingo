import {StyleSheet, Text, View, Dimensions, Image, Alert} from 'react-native';
import React, {useEffect,useState} from 'react';
import styles from '../../assets/styles';
import scanStyles from '../../assets/scanStyle';

const Circular = valor => {
  const image_ruleta = require('../../assets/ruleta.png');
  const DeviceWidth = Dimensions.get('window').width;

  const dataInsta = valor;
  const dataFecha = dataInsta.valor.substring(0,13);
  const numSorteo = dataFecha.substring(0,3);
  const fecha = dataFecha.substring(4,12)

  const valorInsta = dataInsta.valor.substring(13,31);
  let ganador = valorInsta.substring(0, 2);
  let premio1 = valorInsta.substring(2, 4);
  let premio2 = valorInsta.substring(4, 6);
  let premio3 = valorInsta.substring(6, 8);
  let premio4 = valorInsta.substring(8, 10);
  let premio5 = valorInsta.substring(10, 12);
  let premio6 = valorInsta.substring(12, 14);
  let premio7 = valorInsta.substring(14, 16);
  let premio8 = valorInsta.substring(16, 18);

  const mensajes = (props) => {
    Alert.alert("¡Felicidades!",'Eres ganador de' + ' ' + props);
  }
  
  //VALIDACIONES 
  useEffect(() => {
  if(dataInsta && premio1 === ganador){
    mensajes("VEHÍCULO");
  }
  else if(dataInsta && premio2 === ganador){
    mensajes("$ 20.000");
  }
  else if(dataInsta && premio3 === ganador){
    mensajes("$ 10.000");
  }
  else if(dataInsta && premio4 === ganador){
    mensajes("$ 5.000");
  }
  else if(dataInsta && premio5 === ganador){
    mensajes("$ 100");
  }
  else if(dataInsta && premio6 === ganador){
    mensajes("$ 30");
  }
  else if(dataInsta && premio7 === ganador){
    mensajes("$ 15");
  }
  else if(dataInsta && premio8 === ganador){
    mensajes("$ 3");
  }else{
      Alert.alert('¡Lo sentimos!', 'No eres ganador');
  }
}, [dataInsta]);

  return (
    <>
      <View>
        <Text style={scanStyles.textTitle1}>Verificación de premio Instantaneo</Text>
        <Text style={scanStyles.textTitle1}>Sorteo N° {numSorteo}   {fecha}</Text>
      </View>
      <View style={styles.estilosPrincipal}>
      <Image
          source={image_ruleta}
          resizeMode="center"
          style={{
            position: 'absolute',
            width: '100%',
            height: '100%',
          }}
        />
        <View style={styles.borderCuariculas}>
          {/* INICIO COLUMNA 1 */}
          <View>
            <View
              style={{
                width: DeviceWidth * 0.2,
                height: DeviceWidth * 0.2,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'flex-end',
                alignSelf: 'center',
                justifyContent: 'flex-end',
              }}>
              <Text style={styles.estiloNumeros}>{premio1}</Text>
            </View>
            <View
              style={{
                width: DeviceWidth * 0.2,
                height: DeviceWidth * 0.2,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'flex-end',
                alignSelf: 'center',
                justifyContent: 'flex-start',
              }}>
              <Text style={styles.estiloNumeros}>{premio8}</Text>
            </View>
          </View>
          {/* INICIO COLUMNA 2 */}
          <View>
            <View
              style={{
                width: DeviceWidth * 0.2,
                height: DeviceWidth * 0.3,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'flex-end',
              }}>
              <Text style={styles.estiloNumeros}>{premio2}</Text>
            </View>
            <View
              style={{
                width: DeviceWidth * 0.2,
                height: DeviceWidth * 0.3,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}></View>
            <View
              style={{
                width: DeviceWidth * 0.2,
                height: DeviceWidth * 0.3,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'flex-start',
              }}>
              <Text style={styles.estiloNumeros}>{premio7}</Text>
            </View>
          </View>
          {/* INICIO COLUMNA 3 */}
          <View>
            <View
              style={{
                width: DeviceWidth * 0.2,
                height: DeviceWidth * 0.3,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'flex-end',
              }}>
              <Text style={styles.estiloNumeros}>{premio3}</Text>
            </View>
            <View
              style={{
                width: DeviceWidth * 0.2,
                height: DeviceWidth * 0.3,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'flex-start',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Text style={{...styles.estiloNumeros, 
              position: 'absolute',
              top: 30,
              left: -20,
              fontSize: 35}}>{ganador}</Text>
            </View>
            <View
              style={{
                width: DeviceWidth * 0.2,
                height: DeviceWidth * 0.3,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'flex-start',
              }}>
              <Text style={styles.estiloNumeros}>{premio6}</Text>
            </View>
          </View>
          {/* INICIO COLUMNA 4 */}
          <View>
            <View
              style={{
                width: DeviceWidth * 0.2,
                height: DeviceWidth * 0.2,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'flex-start',
                alignSelf: 'center',
                justifyContent: 'flex-end',
              }}>
              <Text style={styles.estiloNumeros}>{premio4}</Text>
            </View>
            <View
              style={{
                width: DeviceWidth * 0.2,
                height: DeviceWidth * 0.2,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'flex-start',
                alignSelf: 'center',
                justifyContent: 'flex-start',
              }}>
              <Text style={styles.estiloNumeros}>{premio5}</Text>
            </View>
          </View>
        </View>
      </View>
    </>
  );
};

export default Circular;
