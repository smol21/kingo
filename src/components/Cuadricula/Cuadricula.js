import {StyleSheet, Text, View, Dimensions, Image, Alert} from 'react-native';
import React, {useEffect, useState} from 'react';
import styles from '../../assets/styles';
import scanStyles from '../../assets/scanStyle';
import {useFocusEffect} from '@react-navigation/native';
import base64 from 'react-native-base64';

const Cuadricula = (valor) => {
  const image_cuadricula = require('../../assets/cuadricula_bingo_2.png');
  const DeviceWidth = Dimensions.get('window').width;
  const resultValorApi = valor.api;
  const resultDecode = base64.decode(valor.api);

  const mensajes = (aciertos, props) => {
    Alert.alert('¡Felicidades! Obtuviste' + ' ' + aciertos,'Eres ganador de' + ' ' + props);
  };

  //DATOS DESDE API
  const arrayDataApi = [];
  if (resultValorApi && resultValorApi != '999' && resultValorApi != '002') {
    const v1 = resultDecode.substring(0, 2);
    const v2 = resultDecode.substring(2, 4);
    const v3 = resultDecode.substring(4, 6);
    const v4 = resultDecode.substring(6, 8);
    const v5 = resultDecode.substring(8, 10);
    const v6 = resultDecode.substring(10, 12);
    const v7 = resultDecode.substring(12, 14);
    const v8 = resultDecode.substring(14, 16);
    arrayDataApi.push(v1, v2, v3, v4, v5, v6, v7, v8);
  }

  //DATOS DESDE ESCANEO
  const dataBingo = (valor);
  const arrayDataEscaneo = [];

  const data = dataBingo.valor.substring(0, 13);
  const numSorteo = data.substring(0, 3);
  const fecha = data.substring(4, 12);

  const valorBingo = dataBingo.valor.substring(13, 29);
  const p1 = valorBingo.substring(0, 2);
  const p2 = valorBingo.substring(2, 4);
  const p3 = valorBingo.substring(4, 6);
  const p4 = valorBingo.substring(6, 8);
  const p5 = valorBingo.substring(8, 10);
  const p6 = valorBingo.substring(10, 12);
  const p7 = valorBingo.substring(12, 14);
  const p8 = valorBingo.substring(14, 16);

  arrayDataEscaneo.push(p1, p2, p3, p4, p5, p6, p7, p8);

  const arrayCuatroEsquinas = [];
  arrayCuatroEsquinas.push(p1, p3, p6, p8);

  const arrayCruzPeque = [];
  arrayCruzPeque.push(p2, p4, p5, p7);

  const arrayLinea1 = [];
  arrayLinea1.push(p1, p2, p3);
  const arrayLinea2 = [];
  arrayLinea2.push(p6, p7, p8);
  const arrayColumna1 = [];
  arrayColumna1.push(p1, p4, p6);
  const arrayColumna2 = [];
  arrayColumna2.push(p3, p5, p8);

  //VALIDACION PARA LINEA 1
  let contLinea1 = 0;
  for (let i = 0; i < arrayDataApi.length; i++) {
    for (let j = 0; j < arrayDataApi.length; j++) {
      if (arrayDataApi[i] == arrayLinea1[j]) {
        contLinea1++;
      }
    }
  }

  //VALIDACION PARA LINEA 2
  let contLinea2 = 0;
  for (let i = 0; i < arrayDataApi.length; i++) {
    for (let j = 0; j < arrayDataApi.length; j++) {
      if (arrayDataApi[i] == arrayLinea2[j]) {
        contLinea2++;
      }
    }
  }

  //VALIDACION PARA COLUMNA 1
  let contCol1 = 0;
  for (let i = 0; i < arrayDataApi.length; i++) {
    for (let j = 0; j < arrayDataApi.length; j++) {
      if (arrayDataApi[i] == arrayColumna1[j]) {
        contCol1++;
      }
    }
  }

  //VALIDACION PARA COLUMNA 2
  let contCol2 = 0;
  for (let i = 0; i < arrayDataApi.length; i++) {
    for (let j = 0; j < arrayDataApi.length; j++) {
      if (arrayDataApi[i] == arrayColumna2[j]) {
        contCol2++;
      }
    }
  }

  //VALIDACION PARA GANADORES 4 ESQUINAS
  let contCruzPeque = 0;
  for (let i = 0; i < arrayDataApi.length; i++) {
    for (let j = 0; j < arrayDataApi.length; j++) {
      if (arrayDataApi[i] == arrayCruzPeque[j]) {
        contCruzPeque++;
      }
    }
  }

  //VALIDACION PARA GANADORES 4 ESQUINAS
  let contCuatroEsquinas = 0;
  for (let i = 0; i < arrayDataApi.length; i++) {
    for (let j = 0; j < arrayDataApi.length; j++) {
      if (arrayDataApi[i] == arrayCuatroEsquinas[j]) {
        contCuatroEsquinas++;
      }
    }
  }
  //VALIDACION PARA GANADORES MAYORES DE 5,6,7,8
  let iguales = 0;
  for (let i = 0; i < arrayDataApi.length; i++) {
    for (let j = 0; j < arrayDataApi.length; j++) {
      if (arrayDataApi[i] == arrayDataEscaneo[j]) {
        iguales++;
      }
    }
  }

  useEffect(() => {
    if (resultValorApi && resultValorApi != '999' && resultValorApi != '002') {
      if (iguales === 8) {
        mensajes('8 aciertos.', '$250.000 + CAMIONETA');
      } else if (iguales === 7) {
        mensajes('7 aciertos.', '$1.000');
      } else if (contCuatroEsquinas === 4) {
        mensajes('4 esquinas.', '$75');
      } else if (contCruzPeque === 4) {
        mensajes('Cruz pequeña.', '$75');
      } else if (iguales === 6) {
        mensajes('6 aciertos', '$50');
      } else if (iguales === 5) {
        mensajes('5 aciertos', '$10');
      } else if (
        contLinea1 === 3 ||
        contLinea2 === 3 ||
        contCol1 === 3 ||
        contCol2 === 3
      ) {
        mensajes('3 aciertos', '$3');
      }
       else{
        Alert.alert('¡Lo sentimos!','No eres ganador');
      } 
    } else if((resultValorApi && resultValorApi === '999') || (resultValorApi && resultValorApi === '002')) {
       Alert.alert('¡AVISO!','No hay resultados disponibles para este sorteo');
    }
  }, [resultValorApi]);

  return (
    <View>
      <View>
        <Text style={scanStyles.textTitle1}>Verificación de premio Bingo</Text>
        <Text style={scanStyles.textTitle1}>
          Sorteo N° {numSorteo} {fecha}
        </Text>
      </View>
      <View style={styles.estilosPrincipal}>
        <Image
          source={image_cuadricula}
          resizeMode="center"
          style={{
            position: 'absolute',
            width: '100%',
            height: '100%',
          }}
        />
        <View style={styles.borderCuariculas}>
          {/* INICIO COLUMNA 1 */}
          <View>
            <View
              style={{
                width: DeviceWidth * 0.2,
                height: DeviceWidth * 0.2,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Text style={styles.estiloNumeros}>{p1}</Text>
            </View>
            <View
              style={{
                width: DeviceWidth * 0.2,
                height: DeviceWidth * 0.2,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Text style={styles.estiloNumeros}>{p4}</Text>
            </View>
            <View
              style={{
                width: DeviceWidth * 0.2,
                height: DeviceWidth * 0.2,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Text style={styles.estiloNumeros}>{p6}</Text>
            </View>
          </View>
          {/* INICIO COLUMNA 2 */}
          <View>
            <View
              style={{
                width: DeviceWidth * 0.2,
                height: DeviceWidth * 0.2,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Text style={styles.estiloNumeros}>{p2}</Text>
            </View>
            <View
              style={{
                width: DeviceWidth * 0.2,
                height: DeviceWidth * 0.2,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Image
                style={styles.tinyLogoMontos}
                source={require('../../assets/corona.png')}
              />
            </View>
            <View
              style={{
                width: DeviceWidth * 0.2,
                height: DeviceWidth * 0.2,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Text style={styles.estiloNumeros}>{p7}</Text>
            </View>
          </View>
          {/* INICIO COLUMNA 3 */}
          <View>
            <View
              style={{
                width: DeviceWidth * 0.2,
                height: DeviceWidth * 0.2,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Text style={styles.estiloNumeros}>{p3}</Text>
            </View>
            <View
              style={{
                width: DeviceWidth * 0.2,
                height: DeviceWidth * 0.2,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Text style={styles.estiloNumeros}>{p5}</Text>
            </View>
            <View
              style={{
                width: DeviceWidth * 0.2,
                height: DeviceWidth * 0.2,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Text style={styles.estiloNumeros}>{p8}</Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Cuadricula;
