import {View, Text, Dimensions, ScrollView, Image} from 'react-native';
import React, {useState} from 'react';
import Header from '../components/Header/Header';
import SocialNetworks from '../components/SocialNetworks/SocialNetworks';
import {Dropdown} from 'react-native-element-dropdown';
import styles from '../assets/styles';
import scanStyles from '../assets/scanStyle';
import {useFocusEffect} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import base64 from 'react-native-base64';

const Resultados = () => {
  const image = require('../assets/fondo_1.png');

  const [tokenSistema, setTokenSistema] = useState('');
  const DeviceWidth = Dimensions.get('window').width;
  const [datos, setDatos] = useState('');
  const [resultados, setResultados] = useState('');

  // const url_api_fecha =
  //   'http://200.44.165.134:8041/triplegordoweb.Servicio.juego/ServicioTGWebJuego.svc/ServicioTGWebJuego/ListarSorteosEscrutadosK';
  const url_api_fecha =
    'https://www.apismol.com.ve/triplegordoweb.servicio.juego/ServicioTGWebJuego.svc/ServicioTGWebJuego/ListarSorteosEscrutadosK';
  // const url_result_premios =
  //   'http://200.44.165.134:8041/triplegordoweb.Servicio.juego/ServicioTGWebJuego.svc/ServicioTGWebJuego/ConsultarResultadosK';
  const url_result_premios =
    'https://www.apismol.com.ve/triplegordoweb.servicio.juego/ServicioTGWebJuego.svc/ServicioTGWebJuego/ConsultarResultadosK';


  const resultDecode = base64.decode(resultados);
  const [isFocus, setIsFocus] = useState(false);

  const sorteo_posicion_0 = datos[0];
  const sorteo_posicion_1 = datos[1];
  const sorteo_posicion_2 = datos[2];
  const sorteo_posicion_3 = datos[3];
  const sorteo_posicion_4 = datos[4];

  const data = [
    {label: sorteo_posicion_0, value: sorteo_posicion_0},
    {label: sorteo_posicion_1, value: sorteo_posicion_1},
    {label: sorteo_posicion_2, value: sorteo_posicion_2},
    {label: sorteo_posicion_3, value: sorteo_posicion_3},
    {label: sorteo_posicion_4, value: sorteo_posicion_4},
  ];

  const [value, setValue] = useState('');

  const getTokenSistema = async () => {
    try {
      let getToken = await AsyncStorage.getItem('token');
      getToken = JSON.parse(getToken);
      setTokenSistema(getToken);
      if (getToken) apiFechaSorteo(getToken);
    } catch (e) {
      console.log('error get token sistema', e);
    }
  };

  const apiFechaSorteo = token => {
    fetch(url_api_fecha, {
      method: 'GET',
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(function (response) {
        if (response.mensaje.code === '000') {
          setDatos(response.datos);
        }
      })
      .catch(error => {
        console.log('errorV', error);
      });
  };

  const apiResultado = (val, fecha) => {
    const url_final = url_result_premios + '/' + val + '/' + fecha;
    fetch(url_final, {
      method: 'GET',
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: 'Bearer ' + tokenSistema,
      },
    })
      .then(response => response.json())
      .then(function (response) {
        console.log("consultar resultados",response)
        if (response.message.code === '000') {
          setResultados(response.data.Bingo);
        } else if (response.message.code === '002') {
          setResultados('');
        }
      })
      .catch(error => {
        console.log('error apiResultado', error);
      });
  };

  const v1 = resultDecode.substring(0, 2);
  const v2 = resultDecode.substring(2, 4);
  const v3 = resultDecode.substring(4, 6);
  const v4 = resultDecode.substring(6, 8);
  const v5 = resultDecode.substring(8, 10);
  const v6 = resultDecode.substring(10, 12);
  const v7 = resultDecode.substring(12, 14);
  const v8 = resultDecode.substring(14, 16);

  // // FECHA
  const sorteo1 = value.substring(0, 3);
  const sorteo = base64.encode(sorteo1);
  const dateSorteo = value.substring(6);
  const dia = dateSorteo.substring(0, 2);
  const mes = dateSorteo.substring(3, 5);
  let anno = dateSorteo.substring(8, 10);
  anno = '20' + anno;
  const fechaSorteoActual = anno + '-' + mes + '-' + dia;
  const fecha_ = new Date(fechaSorteoActual);
  const fechaSorteoEpoch = (fecha_.getTime() + 14400000) / 1000;

  if (value != '') {
    apiResultado(sorteo, fechaSorteoEpoch );
  } else {
  }

  useFocusEffect(
    React.useCallback(() => {
      getTokenSistema();
    }, []),
  );

  return (
    <>
      <Header />
      <View style={{flex: 1}}>
        <Image
          source={image}
          resizeMode="cover"
          style={{
            position: 'absolute',
            width: '100%',
            height: '100%',
          }}
        />
        <View
          style={{
            marginTop: 20,
            marginLeft: 20,
            marginRight: 20,
          }}>
          <Dropdown
            style={[styles.dropdown, isFocus && {borderColor: '#F8CC23'}]}
            placeholderStyle={styles.placeholderStyle}
            selectedTextStyle={styles.selectedTextStyle}
            containerStyle={styles.containerStyle}
            iconStyle={styles.iconStyle}
            iconColor="#044ebd"
            activeColor={styles.activeColor}
            data={data}
            maxHeight={300}
            labelField="label"
            valueField="value"
            placeholder={!isFocus ? 'Sorteos' : '...'}
            value={value}
            onFocus={() => setIsFocus(true)}
            onBlur={() => setIsFocus(false)}
            onChange={item => {
              console.log('item', item.value);
              setValue(item.value);
              setIsFocus(false);
            }}
            disable={datos == [] ? true : false}
          />
        </View>
        <View>
        { resultados ? 
          <Text style={{...scanStyles.textTitle1, paddingTop:30, paddingBottom:0}}>Resultado Sorteo {sorteo1} - {dateSorteo}</Text>
          : <></>
        }
      </View>
        <ScrollView>
          <View style={styles.estilosPrincipal}>
            <View style={{...styles.borderCuariculas, marginTop: 30}}>
              {/* INICIO COLUMNA 1 */}
              <View>
                <View
                  style={{
                    width: DeviceWidth * 0.2,
                    height: DeviceWidth * 0.2,
                    backgroundColor: 'transparent',
                    borderColor: '#fff',
                    borderTopWidth: 4,
                    borderLeftWidth: 4,
                    borderRightWidth: 1,
                    borderBottomWidth: 1,
                    borderTopLeftRadius: 20,
                    flex: 1,
                    alignItems: 'center',
                    alignSelf: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text style={styles.estiloNumeros2}>{v1}</Text>
                </View>
                <View
                  style={{
                    width: DeviceWidth * 0.2,
                    height: DeviceWidth * 0.2,
                    backgroundColor: 'transparent',
                    borderColor: '#fff',
                    borderLeftWidth: 4,
                    borderTopWidth: 1,
                    borderRightWidth: 1,
                    borderBottomWidth: 1,
                    flex: 1,
                    alignItems: 'center',
                    alignSelf: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text style={styles.estiloNumeros2}>{v4}</Text>
                </View>
                <View
                  style={{
                    width: DeviceWidth * 0.2,
                    height: DeviceWidth * 0.2,
                    backgroundColor: 'transparent',
                    borderColor: '#fff',
                    borderBottomWidth: 4,
                    borderLeftWidth: 4,
                    borderTopWidth: 1,
                    borderRightWidth: 1,
                    borderBottomLeftRadius: 20,
                    flex: 1,
                    alignItems: 'center',
                    alignSelf: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text style={styles.estiloNumeros2}>{v6}</Text>
                </View>
              </View>
              {/* INICIO COLUMNA 2 */}
              <View>
                <View
                  style={{
                    width: DeviceWidth * 0.2,
                    height: DeviceWidth * 0.2,
                    backgroundColor: 'transparent',
                    borderColor: '#fff',
                    borderTopWidth: 4,
                    borderBottomWidth: 1,
                    borderLeftWidth: 1,
                    borderRightWidth: 1,
                    flex: 1,
                    alignItems: 'center',
                    alignSelf: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text style={styles.estiloNumeros2}>{v2}</Text>
                </View>
                <View
                  style={{
                    width: DeviceWidth * 0.2,
                    height: DeviceWidth * 0.2,
                    backgroundColor: 'transparent',
                    borderColor: '#fff',
                    borderTopWidth: 1,
                    borderBottomWidth: 1,
                    borderLeftWidth: 1,
                    borderRightWidth: 1,
                    flex: 1,
                    alignItems: 'center',
                    alignSelf: 'center',
                    justifyContent: 'center',
                  }}>
                  <Image
                    style={styles.tinyLogoMontos}
                    source={require('../assets/corona.png')}
                  />
                </View>
                <View
                  style={{
                    width: DeviceWidth * 0.2,
                    height: DeviceWidth * 0.2,
                    backgroundColor: 'transparent',
                    borderColor: '#fff',
                    borderBottomWidth: 4,
                    borderTopWidth: 1,
                    borderLeftWidth: 1,
                    borderRightWidth: 1,
                    flex: 1,
                    alignItems: 'center',
                    alignSelf: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text style={styles.estiloNumeros2}>{v7}</Text>
                </View>
              </View>
              {/* INICIO COLUMNA 3 */}
              <View>
                <View
                  style={{
                    width: DeviceWidth * 0.2,
                    height: DeviceWidth * 0.2,
                    backgroundColor: 'transparent',
                    borderColor: '#fff',
                    borderTopWidth: 4,
                    borderRightWidth: 4,
                    borderBottomWidth: 1,
                    borderLeftWidth: 1,
                    borderTopRightRadius: 20,
                    flex: 1,
                    alignItems: 'center',
                    alignSelf: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text style={styles.estiloNumeros2}>{v3}</Text>
                </View>
                <View
                  style={{
                    width: DeviceWidth * 0.2,
                    height: DeviceWidth * 0.2,
                    backgroundColor: 'transparent',
                    borderColor: '#fff',
                    borderTopWidth: 1,
                    borderRightWidth: 4,
                    borderBottomWidth: 1,
                    borderLeftWidth: 1,
                    flex: 1,
                    alignItems: 'center',
                    alignSelf: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text style={styles.estiloNumeros2}>{v5}</Text>
                </View>
                <View
                  style={{
                    width: DeviceWidth * 0.2,
                    height: DeviceWidth * 0.2,
                    backgroundColor: 'transparent',
                    borderColor: '#fff',
                    borderBottomWidth: 4,
                    borderRightWidth: 4,
                    borderTopWidth: 1,
                    borderLeftWidth: 1,
                    borderBottomRightRadius: 20,
                    flex: 1,
                    alignItems: 'center',
                    alignSelf: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text style={styles.estiloNumeros2}>{v8}</Text>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>

      <SocialNetworks />
    </>
  );
};

export default Resultados;
