import {View, Text,ScrollView} from 'react-native';
import React from 'react';
import Header from '../components/Header/Header';
import SocialNetworks from '../components/SocialNetworks/SocialNetworks';
import styles from '../assets/styles';
import Escaner from './Escaner';

const Instantaneo = () => {
  return (
    <>
      <Header />
      <View style={{flex: 1}}>
        <ScrollView>
          <Escaner />
        </ScrollView>
      </View>

      <SocialNetworks />
    </>
  );
};

export default Instantaneo;
