import {Image, View, ScrollView, TouchableOpacity, Text,Alert,Linking} from 'react-native';
import React, {useState} from 'react';
import Header from '../components/Header/Header';
import SocialNetworks from '../components/SocialNetworks/SocialNetworks';
import styles from '../assets/styles';
import {useNavigation} from '@react-navigation/native';
import {useFocusEffect} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const InicioKingo = () => {
  const image = require('../assets/fondo_inicio.jpg');
  const image2 = require('../assets/logKingo.png')
  const navigation = useNavigation();
  const [disabled, setDisable] = useState(true);

  // const url =
  //   'http://200.44.165.134:8041/triplegordoweb.Servicio.general/ServicioTGWebGeneral.svc/ServicioTGWebGeneral/IniciarSesionApp';

    const url =
    'https://www.apismol.com.ve/triplegordoweb.servicio.general/ServicioTGWebGeneral.svc/ServicioTGWebGeneral/IniciarSesionApp';

  const iniSesion = () => {
    // console.log("Hola logueo")
    fetch(url, {
      method: 'POST',
      body: JSON.stringify({
        login: 'valfredo',
        password: 'abc.123',
        url: 'kingo-oficial.com',
        version: '1.3' /* version */,
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    })
      .then(response => response.json())
      .then(function (response) {
        if (response.message?.code === "000" || response.Message?.code === "000"){
          // console.log('response.token',response.message.code)
          setTokenSistema(response.token);
          setDisable(false);
        }
        else if (response.message.code === "050") {
          Alert.alert(
            '¡Kingo Oficial informa!',
            response.message.description,
            [
              {
                text: 'Actualizar',
                onPress: () => {
                  Linking.openURL(
                    'https://play.google.com/store/apps/details?id=com.kingoOficial',
                  );
                },
              },
            ],
          );
        }
      })
      .catch(error => {
        console.error('error versionApp', error);
      });
  };

  const setTokenSistema = async (variable) => {
    // console.log("var",variable)
    try {
      const valorToken = JSON.stringify(variable);
      await AsyncStorage.setItem('token', valorToken);
    } catch(e) {
      console.log("error setToken sistema",e);
    }
  }


  const getTokenSistema =  async () => {
    try {
      const token = await AsyncStorage.getItem('token')
      deshabilitar_btn(token);
    } catch(e) {
      console.log("error getToken sistema",e)
    }
}

const deshabilitar_btn = (token) => {
  if(token){
    setDisable(false);
  }
}

    useFocusEffect(
    React.useCallback(() => {
      iniSesion();
      getTokenSistema();
    }, []),
  );

  return (
    <>
      {/* <Header /> */}
      <View style={{flex: 1}}>
        <Image
          source={image}
          resizeMode="stretch"
          style={{
            position: 'absolute',
            width: '100%',
            height: '100%',
          }}
        />
        <ScrollView
          style={styles.scrollViewStyle}
          contentContainerStyle={{
            flexGrow: 1,
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 50,
          }}>
            
            <Image
              source={image2}
              resizeMode="center"
              style={{

                justifyContent: 'center',
                alignItems: 'center',
                
                width: '100%',
                height: '50%',
              }}
            />
           
          <View>
            <TouchableOpacity
              disabled={disabled}
              onPress={() => navigation.navigate('Premio')}
              style={disabled ? styles.buttonTouchableGrey : styles.buttonTouchableYellow}>
              <Text style={styles.buttonTextStyleBlue}>VALIDAR PREMIO</Text>
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity
              disabled={disabled}
              onPress={() => navigation.navigate('Resultados')}
              style={disabled ? styles.buttonTouchableGrey : styles.buttonTouchableYellow}>
              <Text style={styles.buttonTextStyleBlue}>CONSULTAR RESULTADOS</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
      <SocialNetworks />
    </>
  );
};

export default InicioKingo;