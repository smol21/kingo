import React, {Fragment, useState} from 'react';
import {
  TouchableOpacity,
  Text,
  Linking,
  View,
  Image,
  ImageBackground,
  BackHandler,
  Alert
} from 'react-native';
import QRCodeScanner from 'react-native-infy-qrcode-scanner';
import styles from '../assets/scanStyle';
import styleBase from '../assets/styles';
import Circular from '../components/Cuadricula/Circular';
import Cuadricula from '../components/Cuadricula/Cuadricula';
import {useNavigation} from '@react-navigation/native';
import {useFocusEffect} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import base64 from 'react-native-base64';
import TituloCaduco from '../components/TituloText/TituloCaduco';

const Escaner = () => {
  const image = require('../assets/fondo_1.png');
  const [tokenSis, setTokenSis] = useState('');
  const navigation = useNavigation();
  // const url_result_premios =
  //   'http://200.44.165.134:8041/triplegordoweb.Servicio.juego/ServicioTGWebJuego.svc/ServicioTGWebJuego/ConsultarResultadosK';
  const url_result_premios =
    'https://www.apismol.com.ve/triplegordoweb.servicio.juego/ServicioTGWebJuego.svc/ServicioTGWebJuego/ConsultarResultadosK'; 
  const tokenState = tokenSis;

  const getTokenSistema = async () => {
    setTimeout(async () => {
      try {
        let getToken = await AsyncStorage.getItem('token');
       
        getToken = JSON.parse(getToken);
        setTokenSis(getToken);
      } catch (e) {
        console.log('error get token sistema', e);
      }
    }, 1000);
  };

  const [state, setState] = useState({
    scan: false,
    ScanResult: false,
    result: null,
    datosSorteo: '',
  });
  const [datosApi, setDatosApi] = useState('');

  const [validarFechaCaduca, setValidarFechaCaduca] = useState(false);

  const apiResultPasivo = (sorteo, fecha) => {
    const url_final = url_result_premios + '/' + sorteo + '/' + fecha;
    fetch(url_final, {
      method: 'GET',
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: 'Bearer ' + tokenState,
      },
    })
      .then(response => response.json())
      .then(function (response) {
        if(response.message.code === '000'){
          setDatosApi(response.data.Bingo)
        }else if(response.message.code === '999'){
              setDatosApi('999');
          }   
          else if(response.message.code === '002'){
            setDatosApi('002');
          }   
      })
      .catch(error => {
        console.log('error apiResultadoPasivo', error);
      });
  };

  const setResultadosBingo = async variable => {
    try {
      const valorResult = JSON.stringify(variable);
      await AsyncStorage.setItem('result_', valorResult);
    } catch (e) {
      console.log('error set Resultado', e);
    }
  };


  const onSuccess = e => {
    state.datosSorteo = e.data;
    if (state.datosSorteo) {
      // console.log('valor tiene algo', state.datosSorteo);
      setState({
        ...state,
        result: e,
        scan: false,
        ScanResult: true,
        datosSorteo: e.data,
      });
    } else {
       Alert.alert('¡No se pudo leer el código QR!','Por favor intente nuevamente');
    }
    //DATA DEL SORTEO
    const dataSorteoActual = state.datosSorteo;
    let numSorteo = dataSorteoActual.substring(0, 3);
    numSorteo = base64.encode(numSorteo);

    //FECHA SORTEO
    const fechaSorteo = dataSorteoActual.substring(4, 12);
    const dia = fechaSorteo.substring(0, 2);
    const mes = fechaSorteo.substring(3, 5);
    let anno = fechaSorteo.substring(6, 8);
    anno = '20' + anno;
    const fechaSorteoActual = anno + '-' + mes + '-' + dia;

    const fecha_ = new Date(fechaSorteoActual);
    const fechaEpoch = (fecha_.getTime() + 14400000) / 1000;

     // FECHA DEL DIA DE HOY
     const date = ('0' + new Date().getDate()).slice(-2);
     const month = ('0' + (new Date().getMonth() + 1)).slice(-2);
     const year = new Date().getFullYear();
     const fechaActual = year + '-' + month + '-' + date;
 
     // RESTA ENTRE LAS FECHAS
     const fechaInicio = new Date(fechaSorteoActual).getTime();
     const fechaFin = new Date(fechaActual).getTime();
     const diff = fechaFin - fechaInicio;
 
     // console.log('difere', diff / (1000 * 60 * 60 * 24));
     const validateFecha = diff / (1000 * 60 * 60 * 24) <= 15;
      setTimeout(() => {
        setValidarFechaCaduca(validateFecha)
      }, 500);


     console.log("validateFecha",validateFecha)
     console.log("validarFechaCaduca",validarFechaCaduca)

    apiResultPasivo(numSorteo, fechaEpoch);
  };
  const activeQR = () => {
    setState({
      ...state,
      scan: true,
    });
    setDatosApi("");
  };
  const scanAgain = () => {
    setState({
      ...state,
      scan: true,
      ScanResult: false,
    });
    setDatosApi("");
  };

    useFocusEffect(
    React.useCallback(() => {
      getTokenSistema();
    }, []),
  );

  return (
    <View>
      <Image
        source={image}
        resizeMode="cover"
        style={{
          position: 'absolute',
          width: '100%',
          height: '100%',
        }}
      />
      <Fragment>
        {!state.scan && !state.ScanResult && (
          <View style={styles.cardView}>
            <Image
              source={require('../assets/camara_blanca.png')}
              style={{height: 35, width: 35}}></Image>
            <Text numberOfLines={8} style={styles.descText}>
              Mueva su cámara{'\n'} sobre el código QR
            </Text>
            <Image
              source={require('../assets/qr.png')}
              style={{margin: 10, width: 250, height: 250}}></Image>
            <TouchableOpacity
              onPress={() => activeQR()}
              style={styles.buttonScan}>
              <View style={styles.buttonWrapper}>
                <Image
                  source={require('../assets/camara_azul.png')}
                  style={{height: 35, width: 35}}></Image>
                <Text style={{...styles.buttonTextStyle, color: '#1F16A1'}}>
                  Escanear código QR
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        )}
        {state.ScanResult && (
          <View>
            {state.datosSorteo.length === 31 ? (
                <View>
                  <TituloCaduco title="¡Ticket caduco!" valor={state.datosSorteo} data={validarFechaCaduca} />
                  <Circular valor={state.datosSorteo} />
                </View>
              
            ) : state.datosSorteo.length === 29 ? (
              <View style={{marginTop:25,marginBottom:25}}>
                <TituloCaduco title="¡Ticket caduco!" valor={state.datosSorteo} data={validarFechaCaduca} />
                <Cuadricula valor={state.datosSorteo} api={datosApi} />  
              </View>
            ) : ( Alert.alert('¡No se pudo leer el código QR!','Por favor intente nuevamente'))
            }
            <View style={{...styles.scanCardView}}>
              {state.datosSorteo.length === 29 ? (
                <TouchableOpacity
                  onPress={() => navigation.navigate('ReglasBingo')}
                  style={styleBase.buttonTouchableYellow}>
                  <Text style={styleBase.buttonTextStyleBlue}>
                    Reglas Kingo
                  </Text>
                </TouchableOpacity>
              ) : (
                <></>
              )}

              <TouchableOpacity
                onPress={() => scanAgain()}
                style={{...styles.buttonScan}}>
                <View style={styles.buttonWrapper}>
                  <Image
                    source={require('../assets/camara_azul.png')}
                    style={{height: 35, width: 35}}></Image>
                  <Text style={{...styles.buttonTextStyle2}}>
                    Escanear de nuevo
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        )}
        {state.scan && (
          <QRCodeScanner
            reactivate={true}
            showMarker={true}
            ref={node => {
              state.scanner = node;
            }}
            onRead={e => onSuccess(e)}
            topContent={
              <View>
                <ImageBackground
                  // source={require('../assets/top-panel.png')}
                  style={styles.topContent}></ImageBackground>
              </View>
            }
            bottomContent={
              <View>
                <ImageBackground
                  // source={require('../assets/bottom-panel.png')}
                  style={styles.bottomContent}>
                  <TouchableOpacity
                    style={styles.buttonScan2}
                    onPress={() => setState({scan: false})}>
                    <Image 
                      source={require('../assets/camara_amarilla.png')}
                      style={{width: 100, height: 100}}></Image>
                  </TouchableOpacity>
                </ImageBackground>
              </View>
            }
          />
        )}
      </Fragment>
    </View>
  );
};
export default Escaner;
