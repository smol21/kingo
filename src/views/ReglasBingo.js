import { FlatList,StyleSheet, Text, View,Image, ScrollView } from 'react-native';
import React from 'react';
import Header from '../components/Header/Header';
import SocialNetworks from '../components/SocialNetworks/SocialNetworks';
import stylesIni from '../assets/styles';

const ReglasBingo = () => {
  const image = require('../assets/fondo_1.png');
  return (
    <>
      <Header />
      <View  style={{flex: 1}}>
      <Image
          source={image}
          resizeMode="cover"
          style={{
            position: 'absolute',
            width: '100%',
            height: '100%',
          }}
        />
      <ScrollView
          style={stylesIni.scrollViewStyle}
          contentContainerStyle={{
            flexGrow: 1,
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 50,
          }}>
        <View style={styles.container}>
        <Text style={{...styles.textTitle, marginBottom: 10}}>Plan de premios</Text>
        <FlatList
          data={[
            {key: '•	Si ocho (8) de las ocho (8) cifras extraídas y cantadas en el sorteo correspondiente coinciden en la Tabla del Bingo, el apostador gana únicamente: USD $250.000+ CAMIONETA.'},
            {key: '•	Si siete (7) de las ocho (8) cifras extraídas y cantadas en el sorteo correspondiente coinciden en la Tabla del Bingo, el apostador gana únicamente: USD $1.000.'},
            {key: '•	Si seis (6) de las ocho (8) cifras extraídas y cantadas en el sorteo correspondiente coinciden en la Tabla del Bingo, el apostador gana únicamente: USD $50).'},
            {key: '•	Si cinco (5) de las ocho (8) cifras extraídas y cantadas en el sorteo correspondiente coinciden en la Tabla del Bingo, el apostador gana únicamente: USD $10.'},
            {key: '•	Cuatro Esquinas de la tabla del BINGO: Si cuatro (4) de las ocho (8) cifras extraídas y cantadas en el sorteo correspondiente forman las Cuatro Esquinas en la Tabla del Bingo, el apostador gana únicamente: USD $75.'},
            {key: '•	Cruz Pequeña de la tabla del BINGO: Si cuatro (4) de las ocho (8) cifras extraídas y cantadas en el sorteo correspondiente forman la Cruz Pequeña en la Tabla del Bingo, el apostador gana únicamente: USD $75.'},
            {key: '•	Línea o Columna: Si tres (3) cifras de las ocho (8) cifras extraídas y cantadas en el sorteo correspondiente forman línea (horizontal) o columna (vertical) en la Tabla del Bingo, el apostador gana únicamente Reintegro: USD $3.'},
          ]}
          renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}
        />
        </View>
         </ScrollView>
      </View>
      <SocialNetworks />
    </>
  );
}

export default ReglasBingo

const styles = StyleSheet.create({
    container: {
     flex: 1,
     padding: 30,
    },
    item: {
      padding: 10,
      fontSize: 15,
      color: '#fff',
    },
    textTitle: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        color: '#fff',
        
      },
  });